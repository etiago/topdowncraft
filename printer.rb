# coding: utf-8
module BoardPrinter
  class PrintUtils
    def initialize
      @board_element_to_char_printer = {
        BoardElement::EMPTY => PrintUtils.simple_char_printer(' '),
        BoardElement::PLAYER => PrintUtils.simple_char_printer('@'),
        BoardElement::WALL => PrintUtils.special_char_printer(97),
        BoardElement::PICKAXE => PrintUtils.simple_char_printer('P'),
        BoardElement::MOUNTAIN => PrintUtils.simple_char_printer('⯭'),
        BoardElement::START_POINT => PrintUtils.simple_char_printer('S'),
        BoardElement::END_POINT => PrintUtils.simple_char_printer('E'),
        BoardElement::ROAD => PrintUtils.simple_char_printer('░'), #PrintUtils.special_char_printer(96),
        BoardElement::DRAGON => PrintUtils.simple_char_printer('Ѫ')
      }
    end

    def get_board_position_tuples(board)
      (0...board.length).to_a.product((0...board[0].length).to_a)
    end

    def draw_board(board, player_position, bwin)
      backing_board = board.backing_board
      positions = get_board_position_tuples(backing_board)
      positions.each { |pos| get_drawer_fn(bwin, backing_board).call(pos) }

      dragon_locations = board.element_locations[BoardElement::DRAGON]
      @board_element_to_char_printer[BoardElement::PLAYER].call(bwin, player_position)
      dragon_locations.each do |dragon_location|
        @board_element_to_char_printer[BoardElement::DRAGON].call(bwin, dragon_location)
      end

      bwin.setpos(backing_board.length - 1, backing_board[0].length - 1)
    end

    def get_drawer_fn(bwin, board)
      lambda do |pos|
        @board_element_to_char_printer[board[pos[0]][pos[1]]].call(bwin, pos)
      end
    end

    def self.simple_char_printer(char)
      lambda do |bwin, pos|
        bwin.attroff(Curses::A_ALTCHARSET)
        bwin.setpos(pos[0], pos[1])
        bwin.addstr(char)
      end
    end

    def self.special_char_printer(char_code)
      lambda do |bwin, pos|
        bwin.attron(Curses::A_ALTCHARSET)
        bwin.setpos(pos[0], pos[1])
        bwin.addch(char_code)
        bwin.attroff(Curses::A_ALTCHARSET)
      end
    end

    private :get_drawer_fn #, :PrintUtils::simple_char_printer, self.:special_char_printer
  end
end
