require 'curses'

require_relative 'model'
require_relative 'constants'
require_relative 'printer'
require_relative 'transformer'

module Game
  class GameController
    attr_accessor :board, :player_position

    def initialize(board_utils, print_utils, board_dimensions, board)
      @board_utils = board_utils
      @print_utils = print_utils
      @board_dimensions = board_dimensions
      @board = board
    end

    def new_player_position_for_pressed_key(current_position, pressed_key)
      if pressed_key == Curses::Key::UP
        [current_position[0] - 1, current_position[1]]
      elsif pressed_key == Curses::Key::DOWN
        [current_position[0] + 1, current_position[1]]
      elsif pressed_key == Curses::Key::LEFT
        [current_position[0], current_position[1] - 1]
      elsif pressed_key == Curses::Key::RIGHT
        [current_position[0], current_position[1] + 1]
      end
    end
    
    def run
      @board = @board_utils.put_wall_around_edge(@board)
      @board = @board_utils.put_start_and_end_points_at_random(@board)
      @board = ((0...4).reduce(@board) { |cur_board, _| @board_utils.generate_road_from_start_to_end_point(cur_board) })
      @board = @board_utils.fill_empty_spaces(@board, BoardElement::MOUNTAIN)
      @board = @board_utils.put_dragon_on_road(@board)
      @board = @board_utils.put_dragon_on_road(@board)
      @board = @board_utils.put_dragon_on_road(@board)
      backing_board = @board.backing_board
      
      @player_position = @board.start_point

      Curses.init_screen
      begin
        height = backing_board.length
        width = backing_board[0].length
        top = (Curses.lines - height) / 2
        left = (Curses.cols - width) / 2
        bwin = Curses::Window.new(height, width, top, left)
        bwin.keypad(true)
        bwin.attron(Curses::A_ALTCHARSET)
        bwin.box(97, 97)
        bwin.attroff(Curses::A_ALTCHARSET)

        stop_game = false
        won = true
        until stop_game
          @print_utils.draw_board(@board, @player_position, bwin)
          bwin.refresh

          pressed_key = bwin.getch
          new_player_position = new_player_position_for_pressed_key(@player_position, pressed_key)
          old_player_position = @player_position
          @player_position = new_player_position if @board_utils.position_is_valid(backing_board, new_player_position)
          @board = Board. new_from_existing_board(existing_board: @board, player_position_override: new_player_position)
          @board = @board_utils.move_dragons(@board)

          if @board.element_locations[BoardElement::DRAGON].include?(@player_position)
            @board.element_locations[BoardElement::DRAGON].include?(old_player_position)
            won = false
            stop_game = true
          else
            stop_game = backing_board[@player_position[0]][@player_position[1]] == BoardElement::END_POINT
          end
        end
        bwin.close
      ensure
        Curses.close_screen
      end
      if won then print 'You win!' else print 'You lose!' end
    end
  end
end

include BoardPrinter
include BoardTransformer
include Game

if __FILE__ == $PROGRAM_NAME
  board_utils = BoardUtils. new
  print_utils = PrintUtils. new
  board_dimensions = [100, 40]
  board = Board. new_from_width_height(board_dimensions)
  game_controller = Game::GameController. new(board_utils, print_utils, board_dimensions, board)
  game_controller.run
end
