module BoardTransformer
  class BoardUtils
    require 'set'
    require_relative 'Logging'
    include Logging

    POSSIBLE_MOVE_MASKS = Array[[1, 0], [-1, 0], [0, 1], [0, -1]]

    def fill_empty_spaces(board, filler_element)
      new_backing_board = board.backing_board
      new_backing_board = new_backing_board.map do |row|
        row.map do |el|
          el == BoardElement::EMPTY ? filler_element : el
        end
      end
      Board. new_from_existing_board(existing_board: board, backing_board_override: new_backing_board)
    end

    def move_dragons(board)
      new_element_locations = board.element_locations
      player_position = board.player_position
      new_dragon_locations = new_element_locations[BoardElement::DRAGON].map do |dragon_location|
        best_move_for_dragon(dragon_location, new_element_locations, player_position)
      end
      new_element_locations[BoardElement::DRAGON] = new_dragon_locations.to_set

      Board. new_from_existing_board(existing_board: board, element_locations_override: new_element_locations)
    end

    def put_dragon_on_road(board)
      new_element_locations = board.element_locations
      road_locations = new_element_locations[BoardElement::ROAD]
      dragon_location = road_locations.to_a.sample
      existing_dragons = new_element_locations.fetch(BoardElement::DRAGON, Set.new)
      new_element_locations[BoardElement::DRAGON] = existing_dragons + Set[dragon_location]
      Board. new_from_existing_board(existing_board: board,
                                     element_locations_override: new_element_locations)
    end

    def generate_road_from_start_to_end_point(board)
      new_backing_board = board.backing_board
      new_element_locations = board.element_locations
      new_end_point = board.end_point
      current_point = board.start_point
      movement_needed_in_x = new_end_point[0] - current_point[0]
      movement_needed_in_y = new_end_point[1] - current_point[1]

      while movement_needed_in_x != 0 || movement_needed_in_y != 0
        x_or_y = -1
        if movement_needed_in_x.zero?
          x_or_y = 1
        elsif movement_needed_in_y.zero?
          x_or_y = 0
        else
          x_or_y = rand(0..1)
        end

        if movement_needed_in_x != 0 && x_or_y.zero?
          step = movement_needed_in_x / movement_needed_in_x.abs
          step = [0, 1, 2, 3].include?(rand(0...10)) == 1 ? step * -1 : step
          movement_needed_in_x -= step
          current_point[0] += step
        elsif movement_needed_in_y != 0 && x_or_y == 1
          step = movement_needed_in_y / movement_needed_in_y.abs
          step = [0, 1, 2, 3].include?(rand(0...10)) ? step * -1 : step
          movement_needed_in_y -= step
          current_point[1] += step
        end
        roads = new_element_locations.fetch(BoardElement::ROAD, Set.new)
        roads << (current_point.clone)
        new_element_locations[BoardElement::ROAD] = roads
        new_backing_board[current_point[0]][current_point[1]] = BoardElement::ROAD
      end
      new_backing_board[current_point[0]][current_point[1]] = BoardElement::END_POINT
      Board. new_from_existing_board(existing_board: board,
                                     backing_board_override: new_backing_board,
                                     element_locations_override: new_element_locations)
    end

    def position_is_valid(board, new_player_position)
      return true if board[new_player_position[0]][new_player_position[1]] == BoardElement::END_POINT
      return true if board[new_player_position[0]][new_player_position[1]] == BoardElement::ROAD

      false
    end

    def start_and_end_points_are_valid(backing_board, start_point, end_point)
      return false if backing_board[start_point[0]][start_point[1]] != BoardElement::EMPTY
      return false if backing_board[end_point[0]][end_point[1]] != BoardElement::EMPTY
      return false if start_point[0] == end_point[0] && start_point[1] == end_point[1]
      return false if (start_point[0] - end_point[0]).abs < 2 || (start_point[1] - end_point[1]).abs < 2

      true
    end

    def put_start_and_end_points_at_random(board)
      start_point = [0, 0]
      end_point = [0, 0]

      new_backing_board = board.backing_board
      until start_and_end_points_are_valid(new_backing_board, start_point, end_point)
        start_point = generate_point_in_board(new_backing_board)
        end_point = generate_point_in_board(new_backing_board)
      end
      new_element_locations = board.element_locations
      new_element_locations[BoardElement::START_POINT] = Set[start_point.clone]
      new_element_locations[BoardElement::END_POINT] = Set[end_point.clone]
      new_backing_board[start_point[0]][start_point[1]] = BoardElement::START_POINT
      new_backing_board[end_point[0]][end_point[1]] = BoardElement::END_POINT

      Board. new_from_existing_board(existing_board: board,
                                     backing_board_override: new_backing_board,
                                     start_point_override: start_point,
                                     end_point_override: end_point,
                                     element_locations_override: new_element_locations)
    end

    def put_wall_around_edge(board)
      backing_board = board.backing_board
      new_backing_board = Array[
        Array.new(backing_board[0].length, 2),
        *(backing_board[1, backing_board.length - 2].map { |row| Array[2, *(row[1, row.length - 2]), 2] }),
        Array.new(backing_board[0].length, 2)]
      Board. new_from_existing_board(existing_board: board, backing_board_override: new_backing_board)
    end

    private

    def legal_positions_for_dragon(dragon_location, element_locations)
      potential_locations = BoardUtils::POSSIBLE_MOVE_MASKS.map do |mask|
        [dragon_location[0] + mask[0], dragon_location[1] + mask[1]]
      end
      potential_locations = potential_locations.filter do |potential_location|
        potential_location != dragon_location &&
          element_locations[BoardElement::ROAD].include?(potential_location)
      end
      potential_locations
    end

    def best_move_for_dragon(dragon_location, element_locations, player_position)
      potential_positions = legal_positions_for_dragon(dragon_location, element_locations)
      sorted_potential_locations = potential_positions.sort do |a, b|
        distance_between(a, player_position) - distance_between(b, player_position)
      end
      sorted_potential_locations[0]
    end

    def distance_between(point_a, point_b)
      Math.sqrt((point_a[0] - point_b[0])**2 + (point_a[1] - point_b[1])**2)
    end

    def generate_point_in_board(backing_board)
      [rand(0...backing_board.length), rand(0...backing_board[0].length)]
    end
  end
end
