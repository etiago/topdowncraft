module BoardElement
  EMPTY = 0
  PLAYER = 1
  WALL = 2
  PICKAXE = 3
  MOUNTAIN = 4
  START_POINT = 5
  END_POINT = 6
  ROAD = 7
  DRAGON = 8
end
