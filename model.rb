class Board
  class << self
    def new_from_width_height(width_height)
      new(width_height: width_height)
    end

    def new_from_existing_board(existing_board:,
                                backing_board_override: nil,
                                start_point_override: nil,
                                end_point_override: nil,
                                element_locations_override: nil,
                                player_position_override: nil)
      new(existing_board: existing_board,
          backing_board_override: backing_board_override,
          start_point_override: start_point_override,
          end_point_override: end_point_override,
          element_locations_override: element_locations_override,
          player_position_override: player_position_override)
    end

    private :new
  end

  def initialize_from_existing_board(existing_board,
                                     backing_board_override = nil,
                                     start_point_override = nil,
                                     end_point_override = nil,
                                     element_locations_override = nil,
                                     player_position_override = nil)
    @backing_board = if backing_board_override.nil? then Marshal.load(Marshal.dump(existing_board.backing_board))
                     else Marshal.load(Marshal.dump(backing_board_override)) end
    @start_point = if start_point_override.nil? then existing_board.start_point
                   else Marshal.load(Marshal.dump(start_point_override)) end
    @end_point = if end_point_override.nil? then existing_board.end_point
                 else Marshal.load(Marshal.dump(end_point_override)) end
    @element_locations = if element_locations_override.nil? then existing_board.element_locations
                         else Marshal.load(Marshal.dump(element_locations_override)) end
    @player_position = if player_position_override.nil? then existing_board.player_position
                       else Marshal.load(Marshal.dump(player_position_override)) end
  end

  def initialize_from_width_height(width_height)
    @backing_board = (0...width_height[1]).map { Array. new(width_height[0], 0) }
    @player_position = [0, 0]
    @start_point = [0, 0]
    @end_point = [0, 1]
    @element_locations = {}
  end

  def initialize(width_height: nil,
                 existing_board: nil,
                 backing_board_override: nil,
                 start_point_override: nil,
                 end_point_override: nil,
                 element_locations_override: nil,
                 player_position_override: nil)
    if !existing_board.nil?
      initialize_from_existing_board(existing_board,
                                     backing_board_override,
                                     start_point_override,
                                     end_point_override,
                                     element_locations_override,
                                     player_position_override)
      freeze
      return
    end

    if !width_height.nil?
      initialize_from_width_height(width_height)
      freeze
    end
  end

  def backing_board
    Marshal.load(Marshal.dump(@backing_board))
  end

  def player_position
    Marshal.load(Marshal.dump(@player_position))
  end

  def start_point
    Marshal.load(Marshal.dump(@start_point))
  end

  def end_point
    Marshal.load(Marshal.dump(@end_point))
  end

  def element_locations
    Marshal.load(Marshal.dump(@element_locations))
  end
end
